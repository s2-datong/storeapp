const base_url = 'https://dealinbackend.herokuapp.com'

export default {

  register: async (username, email, password, phone) => {
    let data = {username, email, password, phone }
    let response = await fetch(base_url + "/user", {method:'POST', body: JSON.stringify(data)})
    return await response.json()
  },

  login: async (username, password) => {
    let data = { password}
    let response = await fetch(base_url + "/user/" + username, {method: 'POST', body: JSON.stringify(data)})
    return await response.json()
  },

  browseProducts: async (sessionToken) => {
    let response = await fetch(base_url + '/product', {method: 'GET', headers: {
    'Authorization': sessionToken} })
    return await response.json()
  },

  placeOrder: async (cart, sessionToken) => {
    let response = await fetch(base_url + '/order', {method:'POST', body:JSON.stringify(cart), headers: {
    'Authorization': sessionToken} })
    return await response.json()
  },

  addProduct: async function(name, description, image, price, quantity, sessionToken){
    let data = {name, description, image, price, quantity}
    let response = await fetch(base_url + '/product', {method: 'POST', body: JSON.stringify(data), headers: {
    'Authorization': sessionToken} })
    return await response.json()
  },

  updateProduct: async function( id, data, sessionToken){
    let response = await fetch(base_url + '/product/' + id, {method: 'PATCH', body: JSON.stringify(data), headers: {
    'Authorization': sessionToken} })
    return await response.json()
  },

  deleteProduct: async function(id, sessionToken){
    let response = await fetch(base_url + '/product/' + id, {method: 'DELETE', headers: {
    'Authorization': sessionToken} })
    return await response.json()
  }
}
