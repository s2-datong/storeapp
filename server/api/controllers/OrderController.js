/**
 * OrderController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
 const emailservice = require('../../services/email.js')
 const smsservice = require('../../services/sms.js')

module.exports = {
  placeOrder: async function(req, res){
      // validate session token
      let user = await User.find({sessionToken: req.headers.authorization}).limit(1)
      if(user.length < 1) return res.json({status: 401, message:'Access denied. Please login'})

      // expect order to be in the form [{id:1, quantity: 1}]
      // validate all product ids and quantity

      // extract productids
      let productIds = []
      req.body.forEach(item => {
        productIds[productIds.length] = item.id
      })
      let count = await Product.count({id: {in: productIds} })

      if(count < productIds.length){
        // there's an invalid product id in the list
        return res.json({status: 404, message:'Order contains a product that does not exist'})
      }

      // Create OrderItems and Order
      let orders = req.body;
      let productids = []
      orders.forEach(ord => { productids[productids.length] = ord.id })

      // attach price
      let products = await Product.find({id: {in: productids}})
      products.forEach(p => {
        orders.forEach( (oitem, index) => {
            if(oitem.id == p.id){
              orders[index].price = p.price
              orders[index].name = p.name
            }
        })
      })

      let orderItems = await OrderItem.createEach( orders ).fetch()
      let orderItemIds = []
      orderItems.forEach(item => { orderItemIds[orderItemIds.length] = item.id })



      let order = await Order.create({user: user.id}).fetch()
      await Order.addToCollection(order.id, 'items').members( orderItemIds )

      res.json({status: 200, message:'Thank you for placing your order. You would get an email shortly'})

      // todo: send email
      let smsmessage = "Order Summary \n"
      let ordertotal = 0;

      orderItems.forEach( (item) => {
        smsmessage += item.name + " " + item.price.toString() + " \n"
        ordertotal += item.price * item.quantity
      })
      await emailservice(user[0].username, user[0].email, orderItems)
      await smsservice(user[0].phone, smsmessage)
  },

  myOrders: async function(req, res){
    let user = await User.find({sessionToken: req.headers.authorization}).limit(1)
    if(user.length < 1) return res.json({status: 401, message:'Access denied. Please login'})

    let result = await User.find({id: user[0].id}).populate('orders')
    orders = result[0].orders;

    let count = orders.length;
    res.json({orders: orders, total: count})
  }

};
