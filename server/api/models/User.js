/**
 * User.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 *
 */

module.exports = {

  attributes: {

    email: {
      type: 'string',
      required: true
    },
    phone: {
      type: 'number',
      required: true
    },
    username: {
      type: 'string',
      required: true,
      unique: true
    },
    password: {
      type: 'string',
      required: true
    },
    sessionToken: {
      type: 'string',
      required: true
    },
    isadmin: {
      type: 'boolean',
      defaultsTo: false
    },
    orders:{
      collection:'order',
      via:'user'
    }

  },

};
