/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also do this by creating a hook.
 *
 * For more information on bootstrapping your app, check out:
 * https://sailsjs.com/config/bootstrap
 */

 const crypto = require('crypto')
 const hash = crypto.createHash('sha256')
 const hash2 = crypto.createHash('sha256')

module.exports.bootstrap = async function(done) {

  // By convention, this is a good place to set up fake data during development.
  //
  // For example:
  // ```
  // // Set up fake development data (or if we already have some, avast)
  // if (await User.count() > 0) {
  //   return done();
  // }
  //
  // await User.createEach([
  //   { emailAddress: 'ry@example.com', fullName: 'Ryan Dahl', },
  //   { emailAddress: 'rachael@example.com', fullName: 'Rachael Shaw', },
  //   // etc.
  // ]);
  // ```

  user_password = hash.update("user", "utf-8")
  userpassword = user_password.digest('hex')

  admin_password = hash2.update("admin", "utf-8")
  adminpassword = admin_password.digest('hex')

  await User.findOrCreate({username:'user'}, { username:'user', password: userpassword, email: 'datong.samwul@gmail.com', phone: 08158144458, isadmin: false, sessionToken: 'abcd1234'})
  await User.findOrCreate({username: 'admin'}, { username: 'admin', password: adminpassword, email:'datong.samwul@gmail.com', phone:08158144458, isadmin: true, sessionToken: '1234abcd'});

  await Product.findOrCreate({name:"Flat Shoe"}, { name:"Flat Shoe", image:"https://picsum.photos/160/100?image=1", description:"Treat your foot right with a flat comfortable footwear", price: 5000, quantity: 20})
  await Product.findOrCreate({name:"Sigma Alpha Bridge"}, { name:"Sigma Alpha Bridge", image: "https://picsum.photos/160/100?image=2", description:"The most addictive Role Playing Game on the planet", price: 12000, quantity: 4})
  await Product.findOrCreate({name: "Funnel Perfume"}, { name: "Funnel Perfume", image: "https://picsum.photos/160/100?image=3", description: "48Hr Protection. Be noticed", price: 2000, quantity: 30})
  await Product.findOrCreate({name: "Greeting card"}, { name: "Greeting card", image: "https://picsum.photos/160/100?image=4", description: "Greeting card for all seasons", price: 800, quantity: 6})
  await Product.findOrCreate({name: "Mustard"}, { name: "Mustard", image: "https://picsum.photos/160/100?image=5", description: "Mustard", price: 700, quantity: 100})


  // Don't forget to trigger `done()` when this bootstrap function's logic is finished.
  // (otherwise your server will never lift, since it's waiting on the bootstrap)
  return done();

};
