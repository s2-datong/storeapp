import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loading: false,
    sessionToken:"",
    products: [

    ],
    cart:[]
  },
  getters: {
    cartSize: state => {
      let count = 0
      state.cart.forEach( p => { count += p.quantity })
      return count
    },
    getOrder: state => {
      let order = []
      state.cart.forEach( p => { order[order.length] = {id: p.id, quantity: p.quantity} })
    },
    sessionToken: state => {
      if(state.sessionToken == ""){ return null; }
      return state.sessionToken
    },
    loading: state => {
      return state.loading
    },
    allProducts: state => {
      return state.products
    }
  },
  mutations: {
    TOGGLE_LOADING: (state) => {
      state.loading = !state.loading
    },
    SET_SESSION: (state, session) => {
      state.sessionToken = session
    },
    ADD_CART: (state, ix) => {

      // prevent adding product twice
      let product = state.products[ix]

      let alreadyincart = false
      state.cart.forEach( (p, pindex) => {
        if(p.id == product.id){
          // update quantity
          alreadyincart = true
          if(p.quantity == product.quantity){ return; }
          state.cart[pindex].quantity++;
        }
      })

      if(alreadyincart == false){
        // add product to cart
        let cartProduct = {}
        Object.assign(cartProduct, product)
        state.cart.push(cartProduct)
        // set quantity to 1
        state.cart[ state.cart.length - 1].quantity = 1;
      }
    },

    REMOVE_CART: (state, index) => {
      state.cart.splice(index, 1)
    },

    CLEAR_CART: (state) => {
      state.cart.splice(0, state.cart.length)
    },

    UPDATE_PRODUCT: (state, products) => {
      state.products.splice(0,state.products.length)
      products.forEach(p => { state.products.push(p)})
    },

    ADMIN_UPDATE_PRODUCT: (state, data) => {
      let i;
      for(i = 0; i < state.products.length; i++){
        let product = state.products[i]
        if(product.id == data.id){
          state.products[i].name = data.name
          state.products[i].description = data.description
          state.products[i].image = data.image
          state.products[i].price = data.price
          state.products[i].quantity = data.quantity
        }
      }
    },

    REMOVE_PRODUCT: (state, id) => {
      let i;
      for(i = 0; i < state.products.length; i++){
        let product = state.products[i]
        if(product.id == id){
          state.products.splice(i, 1);
        }
      }
    },

    ADD_PRODUCT: (state, product) => {
      state.products.push(product)
    },

    LOGOUT: (state) => {
      state.sessionToken = ""
    },

    UP_QUANTITY: (state, index) => {
      let item = state.cart[index]
      let i;
      for(i = 0; i < state.products.length; i++){
        let product = state.products[i];
        if(product.id == item.id){
          if(item.quantity == product.quantity){ return; }
        }
      }


      state.cart[index].quantity += 1;
    },

    DOWN_QUANTITY: (state, index) => {
      if( state.cart[index].quantity == 1){ return; }
      state.cart[index].quantity -= 1;
    },
  },
  actions: {

  }
})
