/**
 * OrderItem.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    id: {type: 'number', required: true },
    price: {type:'number', required: true},
    quantity: {type: 'number', required: true },
    order: { model: 'order'}
  },

};
