const nodemailer = require('nodemailer')
const fs = require('fs')
const aws = require('aws-sdk')

aws.config.loadFromPath( __dirname + '/config.json');

const transporter = nodemailer.createTransport({
      SES: new aws.SES({apiVersion: '2010-12-01'})
  });

function getTemplate( dictionary ){

  return new Promise((resolve, reject) => {
			fs.readFile(  __dirname + '/template.html', 'utf8', (err, data) => {
				if(err) reject(err)
        let message = data;

        let keys = Object.keys(dictionary)
        for(i in keys){
    			let key = keys[i];
    			message = message.replace(new RegExp(key, 'g'), dictionary[key]);
    		}
				resolve(message)
			})
		})
}

function ordersToHtml( orders){
    let html = "";
    orders.forEach(order => {
        html += "<tr>" +
        "<td> <img src='" + order.image + "' style='height:100px;' /> </td>" +
        "<td>" + order.name + "</td>" +
        "<td>" + order.price + "</td>" +
        "<td>" + order.quantity + "</td>" +
        "<td>" + order.price * order.quantity + "</td>" +
        "</tr>"
    })

    return html;
}

async function sendemail(name, email, orders, total){
    let orderhtml = ordersToHtml(orders)
    let template = await getTemplate({"%name%": name, "%orders%": orderhtml, "%total%": total })
    let mailOptions = {
        from: "hello@flipapi.com",
        to: email,
        subject: "Your Order Summary",
		    html: template
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
		    console.log(info);
    });
  }

module.exports = sendemail
