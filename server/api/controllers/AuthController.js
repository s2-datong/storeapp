/**
 * AuthController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const crypto = require('crypto')

module.exports = {

  register: async function(req, res){
    try{
      let hash = crypto.createHash('sha256')
      hashed_password = hash.update(req.body.password, "utf-8")
      password_digest = hashed_password.digest('hex')
      req.body.password = password_digest

      // generate session token
      sessiontoken = Math.random().toString(36).substring(2, 16)
      req.body.sessionToken = sessiontoken

      await User.create( req.body )
      res.json({status: 200, message: 'You have now registered'})
    }
    catch(e){
      res.json({message: e.message})
    }
  },

  login: async function(req, res){
    try{
      let hash = crypto.createHash('sha256')
      hashed_password = hash.update(req.body.password, "utf-8")
      digest = hashed_password.digest('hex')

      let where = { or:[
          {email: req.params.username},
          {username: req.params.username}
        ], password: digest};

      let user = await User.findOne(where)

      if(user){
        return res.json({status: 200, sessionToken: user.sessionToken, message:'login successful', isadmin: user.isadmin})
      }

      return res.json({status: 401, message: 'Access Denied'})
    }catch(e){
      console.log(e)
      return res.json({status: 401, message: 'Access Denied'})
    }
  },

  logout: async function(req, res){
    let user = await User.findOne({sessionTokem: req.headers.authoriztion})
    if(!user) return res.json({status: 200, message:'No session to logout'})

    token = Math.random().toString(36).substring(2, 16)
    await User.update({id: user.id}).set({sessionToken: token})

    res.json({status: 200, message:'You have logged out'})
  }
};
