/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {


  '/': {
    view: 'pages/homepage'
  },

  'post /user': 'AuthController.register',
  'post /user/:username': 'AuthController.login',
  'get /logout': 'AuthController.logout',
  'post /order': 'OrderController.placeOrder',
  'get /order': 'OrderController.myOrders'

};
