const querystring = require('querystring');
const https       = require('https');

const username = "dealtestsam"; // process.env.SMS_USERNAME
const apikey   = "bf168be3552f164b4dc7fa87643397c838bd130e07388ac24e186b0464375568"; // process.env.SMS_APIKEY

function sendMessage(to, message) {

  return new Promise(function(resolve, reject){
      let post_data = querystring.stringify({
          'username' : username,
          'to'       : "+234" + to.toString(),
          'message'  : message
      });

      let post_options = {
          host   : 'api.africastalking.com',
          path   : '/version1/messaging',
          method : 'POST',

          rejectUnauthorized : false,
          requestCert        : true,
          agent              : false,

          headers: {
              'Content-Type' : 'application/x-www-form-urlencoded',
              'Content-Length': post_data.length,
              'Accept': 'application/json',
              'apikey': apikey
          }
      };

      var post_req = https.request(post_options, function(res) {
          res.setEncoding('utf8');
          let data = "";
          res.on('data', function (data) {
              console.log(data)
              resolve( JSON.parse(data))
          })
      });

      // Add post parameters to the http request
      post_req.write(post_data);

      post_req.end();
  })
}

module.exports = sendMessage;
