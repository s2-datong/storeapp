/**
 * ProductController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  create: async function(req, res){
    let user = await User.find({sessionToken: req.headers.authorization}).limit(1)
    if(user.length < 1) return res.json({status: 401, message:'Access denied. Please login'})
    if(user[0].isadmin !== true) return res.json({status: 401, message:'Access denied. Please login'})

    if(!req.body.name || !req.body.description || !req.body.price || !req.body.quantity || !req.body.image){
      return res.json({status: 404, message:'Missing field (name, description, price, quantity, image)'})
    }

    let product = await Product.create({name: req.body.name, description: req.body.description, image: req.body.image,
    price: req.body.price, quantity: req.body.quantity }).fetch()

    res.json({status: 200, message: 'Product added', product: product})
  },

  update: async function(req, res){
    let user = await User.find({sessionToken: req.headers.authorization}).limit(1)
    if(user.length < 1) return res.json({status: 401, message:'Access denied. Please login'})
    if(user[0].isadmin !== true) return res.json({status: 401, message:'Access denied. Please login'})

    let product = await Product.findOne({id: req.params.id})
    let set = {}
    if(req.body.name) set.name = req.body.name
    if(req.body.description) set.description = req.body.description
    if(req.body.price) set.price = req.body.price
    if(req.body.quantity) set.quantity = req.body.quantity
    if(req.body.image) set.image = req.body.image

    if(Object.keys(set).length > 0) await Product.update({id: product.id}).set( set )

    res.json({status: 200, message: 'Product updated'})
  },

  destroy: async function(req, res){
    let user = await User.find({sessionToken: req.headers.authorization}).limit(1)
    if(user.length < 1) return res.json({status: 401, message:'Access denied. Please login'})
    if(user[0].isadmin !== true) return res.json({status: 401, message:'Access denied. Please login'})

    await Product.destroy({id: req.params.id})
    res.json({status: 200, message: 'Product Destroyed'})
  },
};
